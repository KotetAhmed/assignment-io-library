section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
    jz  .end
    inc rax
    jmp .loop
.end:
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
     push rdi
    call string_length
    mov rdx, rax
    mov rax, 1  
    mov rsi, rdi 
    mov rdi, 1
    syscall 
    pop rdi
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push 0
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, rsp					
	mov r10, 10					
	mov rax, rdi				
	push 0						
.loop:
	xor rdx, rdx				
	div r10				
	add rdx, 0x30				
	dec rsp				
	mov byte[rsp], dl			
	cmp rax, 0					
	jnz .loop	
	
	mov rdi, rsp				
	push r9						
	call print_string			
	pop r9						
	mov rsp, r9					
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
      push rbx
        push rdx

        xor rax, rax
        xor rcx, rcx
        xor rdx, rdx
        xor rbx, rbx
.loop:
        mov  bl, [rdi + rcx]
        mov  dl, [rsi + rcx]
        cmp bl, 0x0
        je .res
        cmp dl, 0x0
        je .res
        cmp bl, dl
        jne .res
        inc rcx
        jmp .loop
.res:
        xor rcx, rcx
        cmp bl, dl
        je .yes
        pop rdx
        pop rbx
        mov rax, 0
        ret
.yes:
        pop rdx
        pop rbx
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
   xor rax, rax
    push rax
    mov rdx, 1
    mov rdi, 0
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi 
    mov r13, rsi 
    dec r13 	
    xor r14, r14  
    .check1:
        call read_char 
        cmp rax, 0 
        je .end
        cmp r14, r13 
        je .error
        cmp r14, 0
        jne .check2

        cmp rax, 0x20 
        je .check1
        cmp rax, 0x9 
        je .check1
        cmp rax, 0xA 
        je .check1
    
    .check2:
        cmp rax, 0 
        je .end
        cmp rax, 0x20 
        je .end
        cmp rax, 0x9 
        je .end
        cmp rax, 0xA 
        je .end
        mov [r12+r14], al 
        inc r14
        jmp .check1 

    .error:
        xor rax, rax
        pop r14
        pop r13 
        pop r12
        ret

    .end:  
        mov byte[r12+r14], 0 
        mov rdx, r14
        mov rax, r12
        pop r14
        pop r13 
        pop r12
        ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length          
    mov rsi, rax                
    xor rcx, rcx                
    xor rdx, rdx                
    xor rax, rax                
    xor r8, r8                  
.loop:
    mov r8b, byte [rdi + rcx]   
    cmp r8b, 0                  
    je .end                     
    cmp r8b, 57                 
    ja .end                     
    cmp r8b, 48                 
    jb .end                     
    sub r8b, 48                 
    imul rax, 10                
    add rax, r8                 
    inc rdx                     
    inc rcx                     
    jmp .loop                   
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   .check_sign:
        cmp byte[rdi], '-'
        jne parse_uint
    .invert:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        neg rax
        inc rdx
    .end:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax 
    xor r11, r11  
    push rdi
    push rsi
    push rdx
    call string_length 
    pop rdx 
    pop rsi
    pop rdi
    inc rax
    cmp rax, rdx 
    jnz .error

    .loop:
        mov cl, byte[rdi+r11]
        mov byte[rsi+r11], cl
        inc r11
        cmp r11, rax 
        jnz .loop
        dec rax
        ret

    .error:
        mov rax, 0 
        ret


